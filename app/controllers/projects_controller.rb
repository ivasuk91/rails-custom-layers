class ProjectsController < ApplicationController
  before_action :set_project, only: %i[show edit update destroy]

  def index
    # TODO: Refactor to query
    @projects = Project::AllForUser.new(current_user).call
  end

  def show
    # TODO: Refactor to policy
  end

  def new
    # TODO: Refactor to form
    @project = Project.new
  end

  def create
    # TODO: Refactor to form & policy

    if policy.allows?
      @project = Project::Create.new(project_params, current_user).call
      if @project.save
        redirect_to project_path(@project), notice: 'Project was successfully created.'
      else
        render :new
      end
    else
      redirect_to projects_path, alert: "Your #{current_user.plan} plan is over limited. Please increase it for creating more projects"
    end
  end

  def update
    # TODO: Refactor to form
    if @project.update(project_params)
      redirect_to @project, notice: 'Project was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @project.destroy
    redirect_to projects_url, notice: 'Project was successfully destroyed.'
  end

  private

  def set_project
    @project = current_user.projects.find(params[:id])
  end

  def project_params
    params.require(:project).permit(:name, :user_id)
  end

  def policy
    Project::CreatePolicy.new(current_user)
  end
end
