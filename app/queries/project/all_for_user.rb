class Project
  class AllForUser
    # TODO: Implement
    # should I use here current user?
    def initialize(current_user)
      @user = current_user
    end

    def call
      @user.projects.all
    end
  end
end
