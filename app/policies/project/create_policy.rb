class Project
  class CreatePolicy
    attr_reader :current_user

    def initialize(current_user)
      @user = current_user
    end

    def allows?
      free_plan? || business_plan? || premium_plan? || custom_plan?
    end

    private

    def free_plan?
      @user.plan == 'free' && @user.projects.count < 3
    end

    def business_plan?
      @user.plan == 'business' && @user.projects.count < 10
    end

    def premium_plan?
      @user.plan == 'premium' && @user.projects.count < 100
    end

    def custom_plan?
      @user.plan == 'custom'
    end
  end
end
