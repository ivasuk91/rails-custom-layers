class Project
  class Create
    def self.call(*args)
      new(*args).call
    end

    def initialize(params = {}, current_user)
      @name = params[:name]
      @user_id = current_user.id
      @user = current_user
    end

    def call
      Project.create(project_attr)
    end

    private

    attr_reader :name, :user_id

    def project_attr
      { name: name, user_id: user_id }
    end
  end
end
